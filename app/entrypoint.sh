#!/bin/bash

if [ "$DATABASE" = "testdb" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $ENV_DB_POSTGRES_HOST $ENV_DB_POSTGRES_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi
 
python3 manage.py flush --no-input
# python3 manage.py collectstatic
python3 manage.py migrate

exec "$@"