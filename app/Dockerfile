FROM python:3.10.2-slim-buster as base

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1 \
    PYTHONUNBUFFERED 1

# set work directory
WORKDIR /usr/src/app

# install dependencies
RUN apt-get update \
    && apt-get install -y \
    curl \
    python3-dev \
    python3-pip \
    libpq-dev \
    && curl -sSL https://install.python-poetry.org | python - --version 1.3.2
 
ENV PATH="/root/.local/bin:$PATH"

COPY pyproject.toml poetry.lock /usr/src/app/

RUN poetry config virtualenvs.create false \
    && poetry install --without dev --no-interaction --no-ansi

# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/app/entrypoint.sh

# copy project
COPY . /usr/src/app/

EXPOSE 8000

ENTRYPOINT [ "/usr/src/app/entrypoint.sh" ]