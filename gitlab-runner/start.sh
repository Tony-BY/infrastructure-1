#!/bin/bash

while getopts t:d: flag
do
    case "${flag}" in
        t) TOKEN=${OPTARG};;
        d) DESCRIPTION=${OPTARG};;
    esac
done

echo "token: $TOKEN"
echo "description: $DESCRIPTION"

mkdir -p config

docker-compose up -d

docker-compose exec gitlab-runner-container \
    gitlab-runner register \
    --non-interactive \
    --url https://gitlab.com/ \
    --registration-token $TOKEN \
    --executor docker \
    --description "$DESCRIPTION" \
    --docker-image "docker:stable" \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock
